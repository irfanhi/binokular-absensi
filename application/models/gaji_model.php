<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gaji_model extends CI_Model {

	public function get_karyawan()
	{
		return $this->db->join('kategori','kategori.id_kategori = karyawan.id_kategori')
						->get('karyawan')
						->result();
	}

	public function get_kategori()
	{
		return $this->db->get('kategori')
						->result();
	}

	public function get_data_karyawan_by_id($id)
	{
		return $this->db->where('id_karyawan', $id)
						->get('karyawan')
						->row();
	}

	public function getDataGaji()
	{
		return $this->db->join('karyawan','karyawan.id_karyawan=absensi.id_karyawan')						
						->get('absensi')
						->result();
	}

	public function tambah()
	{
		$data = array(
		
				'karyawan' 	=> $this->input->post('karyawan'),
				'gaji'		=> $this->input->post('gaji'),
				
			);

		$this->db->insert('karyawan', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function hitung_gaji() 
	{
		$query = 'SELECT ab.absensi,
		                 SUBSTRING(ab.tgl_absensi, 1, 7) AS bulan,
		                 COUNT(ab.absensi) AS total_absensi,
		                 kw.id_karyawan,
		                 kw.gaji,
					     kw.karyawan
			        FROM absensi ab
			   RIGHT JOIN karyawan kw
			          ON kw.id_karyawan = ab.id_karyawan
			       WHERE ab.absensi = "yes"
			    GROUP BY bulan, kw.id_karyawan
		';

		$rekap_absensi = $this->db->query($query)->result();
		return $rekap_absensi;
	}
	
	public function get_gaji_by_karyawan_id($id_karyawan) 
	{
		$query = 'SELECT ab.absensi,
		                 SUBSTRING(ab.tgl_absensi, 1, 10) AS tanggal,
		                 kw.id_karyawan,
		                 kw.gaji,
					     kw.karyawan
			        FROM absensi ab
			   RIGHT JOIN karyawan kw
			          ON kw.id_karyawan = ab.id_karyawan
			       WHERE ab.absensi = "yes"
			         AND kw.id_karyawan = ' . $id_karyawan;
        $query .= ' ORDER BY tanggal ASC';
        
		$rekap_absensi = $this->db->query($query)->result();
		return $rekap_absensi;
	}

	public function bayar() {
	{
		$tgl_absensi = date("Y-m-d"); 
		$data = array(
		
				'id_karyawan' 		    => $this->input->get('id_karyawan'),
				'bulan'        			=> $this->input->get('bulan'),
				'tgl_absensi'			=> $tgl_absensi,
				'total_gaji'			=> $this->input->get('total_gaji')
				
			);

		$this->db->insert('gaji', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}


	}

	public function cek_pembayaran($id_karyawan, $bulan) {
		$array = array('id_karyawan' => $id_karyawan, "bulan" => $bulan);
		$check = $this->db->where($array)->get('gaji')->row();
		if (count($check) > 0) {
			return True;
		} else {
			return False;
		}

	}

	public function getBayarGaji(){
		
		$query = 'SELECT g.id_gaji,
		                 g.tgl_absensi,
		                 g.bulan,
		                 kw.id_karyawan,
		                 g.total_gaji,
					     kw.karyawan
			        FROM gaji g
			  INNER JOIN karyawan kw
			          ON kw.id_karyawan = g.id_karyawan 
		';

		$rekap = $this->db->query($query)->result();
		return $rekap;

		//return $this->db->join('pegawai','pegawai.id_pegawai = pembayaran.id_pegawai')
		//				->get('pembayaran')
		//				->result();
	}

}


/* End of file Buku_model.php */
/* Location: ./application/models/Buku_model.php */
/* End of file baju_model.php */
/* Location: ./application/models/baju_model.php */
