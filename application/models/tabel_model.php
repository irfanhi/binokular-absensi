<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tabel_model extends CI_Model {

	public function get_data_absensi()
	{
		return $this->db->join('karyawan','karyawan.id_karyawan=absensi.id_karyawan')
						->get('absensi')
						->result();
	}

	public function get_absensi($a)
	{
		return $this->db->join('karyawan','karyawan.id_karyawan=absensi.id_karyawan')
						->where('id_karyawan',$a)
						->get('absensi')
						->row();
	}

	public function get_data_absensi_by_id($id)
	{
		return $this->db->where('id_absensi', $id)
						->get('absensi')
						->row();
	}

	public function hapus()
	{
		$this->db->where('id_absensi', $this->input->post('hapus_id'))
				 ->delete('absensi');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

		public function getDataTop() {

		$query = 'SELECT absensi.id_absensi,
						 absensi.tgl_absensi,
						 COUNT(absensi.id_karyawan) AS abs,
						 COUNT(absensi.id_absensi) AS total_absensi, 
						 us.id_karyawan,
		                 us.karyawan
		            FROM absensi absensi 
		       RIGHT JOIN karyawan us
		       		  ON us.id_karyawan = absensi.id_karyawan  
		        GROUP BY absensi.id_karyawan 
				ORDER BY COUNT(absensi.id_karyawan)  DESC
				   LIMIT 5
		';

		$rekap_absensi = $this->db->query($query)->result();
		return $rekap_absensi;
		

}
}