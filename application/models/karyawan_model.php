<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan_model extends CI_Model {

	public function get_karyawan()
	{
		return $this->db->join('kategori','kategori.id_kategori = karyawan.id_kategori')
						->get('karyawan')
						->result();
	}

	public function get_kategori()
	{
		return $this->db->get('kategori')
						->result();
	}
	public function get_data_karyawan_by_id($id)
	{
		return $this->db->where('id_karyawan', $id)
						->get('karyawan')
						->row();
	}

	public function tambah($foto)
	{
		$data = array(
	
				'karyawan' 				=> $this->input->post('karyawan'),
				'id_kategori'			=> $this->input->post('kategori'),
				'no_telepon'			=> $this->input->post('no_telepon'),
				'gaji'					=> $this->input->post('gaji'),
				'foto'					=> $foto['file_name']
			);

		$this->db->insert('karyawan', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function ubah()
	{
		$data = array(
				
				'karyawan' 		=> $this->input->post('ubah_karyawan'),
				'id_kategori'	=> $this->input->post('ubah_kategori'),
				'no_telepon'	=> $this->input->post('no_telepon'),
				'gaji'	=> $this->input->post('gaji'),
			);

		$this->db->where('id_karyawan', $this->input->post('ubah_id_karyawan'))
				 ->update('karyawan', $data);
		
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function hapus()
	{
		$this->db->where('id_karyawan', $this->input->post('hapus_id'))
				 ->delete('karyawan');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}


/* End of file baju_model.php */
/* Location: ./application/models/baju_model.php */
