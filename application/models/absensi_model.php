<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class absensi_model extends CI_Model {

	public function get_absensi()
	{
		return $this->db->get('absensi')
						->result();
	}

	public function get_data_karyawan()
	{
		return $this->db->get('karyawan')
						->result();
	}

	public function get_data_absensi_by_id($id)
	{
		return $this->db->where('id_absensi', $id)
						->get('absensi')
						->row();
	}

	public function tambah()
	{
		$jam= Date("H:i:s");

		$data = array(
				'id_karyawan'	=> $this->input->post('id_karyawan'),
				'tgl_absensi'	=> $this->input->post('tgl_absensi'),
				'absensi'		=> $this->input->post('absensi'),
				'jam' =>$jam

		);

	 	return $this->db->insert('absensi',$data);

	 	if($this->db->affected_rows() > 0) {
			return TRUE;
		} else  {
			return FALSE;
     	}

	}

	public function cek_sudah_absen($id_karyawan, $tgl_absensi) 
	{
		$array = array('id_karyawan' => $id_karyawan, "tgl_absensi" => $tgl_absensi);
		$check = $this->db->where($array)->get('absensi')->row();
		
		if (count($check) > 0) {
			return True;
		} else {
			return False;
		}

	}

	
}