<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class absensi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('absensi_model');
	}

	public function index()
	{
		
		if($this->session->userdata('logged_in') == TRUE)

		{

		$data['main_view'] = 'absensi';
		$data['data_karyawan'] = $this->absensi_model->get_data_karyawan();
		$this->load->view('template', $data);

		} else {
			redirect('login/index');
		}
	}

	public function tambah() 
	{

		/**
		echo "<pre>";
		print_r($this->input->post());
		echo "</pre>";
		exit;
		*/
		$tgl_absensi = $this->input->post('tgl_absensi');
		$today = date("Y-m-d");
		# print("tanggal: " . $tgl_absensi . ", today: " . $today);	
		
		if ($tgl_absensi > $today) {
			// tanggal di masa depan, tdk valid disimpan ke db
			
			$this->session->set_flashdata('notif', 'Date in future. not allowed');
			redirect('absensi/index');

		//} if ($tgl_absensi < $today) {
		//	// tanggal di masa lampau, tdk valid disimpan ke db
        //
		//	$this->session->set_flashdata('notif','Data in past. not allowed ');
		//	redirect('absensi/index');

		} else {
	
			$id_karyawan = $this->input->post('id_karyawan');
			if ($this->absensi_model->cek_sudah_absen($id_karyawan, $tgl_absensi) == True) {
				// terjadi duplikasi, pegawai sudah diinput di tgl itu
				$this->session->set_flashdata('notif', 'Pegawai sudah diinput utk tgl tsb');
				redirect('absensi/index');

			} else  {
				// boleh dilanjut insert ke db
				if($this->absensi_model->tambah()) {
					$this->session->set_flashdata('notif', 'Tambah Absensi berhasil');
					redirect('absensi/index');
				} else {
					$this->session->set_flashdata('notif', 'Tambah Absensi gagal');
					redirect('absensi/index');
				}

			}

		}

	}
}	

/* End of file Kasir.php */
/* Location: ./application/controllers/Kasir.php */