<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gaji extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('gaji_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data['main_view'] = 'gaji';
			$data['karyawan'] = $this->gaji_model->get_karyawan();
			$data['gaji']=$this->gaji_model->hitung_gaji();
			//get_kategori untuk dropdown tambah/edit karyawan
			$data['kategori'] = $this->gaji_model->get_kategori();
			$data['tampil_bayar'] = $this->gaji_model->getBayarGaji();
			$this->load->view('template', $data);

		} else {
			redirect('login/index');
		}
	}


	public function bayar () 
	{
		$data['main_view'] = 'gaji';
		$data['gaji'] = $this->gaji_model->hitung_gaji();
		//$data['nama'] = 'Jallo';
		//$data['gaji']=
		//echo "<pre>";
		//print_r($this->input->post());
		//print_r($gaji);
		//echo "</pre>";
		//exit;
		//$this->gaji_model->hitung_gaji();
  		$this->load->view('template', $data);

	}

	public function bayar2 () {

    $id_karyawan = $this->input->get('id_karyawan');
    $bulan   	 = $this->input->get('bulan');
    if ($this->gaji_model->cek_pembayaran($id_karyawan, $bulan) == TRUE) {
      // terjadi duplikasi, pegawai sudah diinput di tgl itu
      $this->session->set_flashdata('notif', 'Sudah dibayar pada tanggal tersebut');
      redirect('gaji');

    } else {
      // boleh dilanjut insert ke db
      if($this->gaji_model->bayar()) {
          $this->session->set_flashdata('notif', 'Pembayaran berhasil');
          redirect('gaji');
        } else {
          $this->session->set_flashdata('notif', 'Pembayaran gagal');
          redirect('gaji');
        }
      }         

    //$this->load->view('template', $data);

  }

	public function karyawan_id($id) 
	{

		if($this->session->userdata('logged_in') == TRUE){
			$detil_gaji = $this->gaji_model->get_gaji_by_karyawan_id($id);
			$data['show_detil'] = "";
			$total = 0;
			$no = 1;
			$data['show_detil'] .= '<table class="table table-striped">
									<tr>
										<th>No</th>
										<th>Id Karyawan</th>
										<th>Nama</th>
										<th>Tanggal</th>
										<th>Absensi</th>
									</tr>';

			foreach ($detil_gaji as $d) {
				$data['show_detil'] .= '<tr>
									<td>'.$no.'</td>
									<td>'.$d->id_karyawan.'</td>
									<td>'.$d->karyawan.'</td>
									<td>'.$d->tanggal.'</td>
									<td>'.$d->absensi.'</td>
								</tr>';
				$no++;
			}

			$total = ($no - 1);
			$data['show_detil'] .= '<tr>
								<td>&nbsp;</td>
								<td>'.$total.' hari</td>
								<td>Gaji: Rp. '.number_format($d->gaji).'</td>
								<td>Tunjangan: Rp. '.number_format($total * 20000).'</td>
								<td>TOTAL: Rp. '.number_format($d->gaji + ($total * 20000)).'</td>
							</tr>';


			$data['show_detil'] .= '</table>';
			echo json_encode($data);
			
		} else {
			redirect('login/index');
		}
	}

	public function cetak_gaji($id)
	{
		// ambil data dari database
		// simpan ke dalam variable
		// panggil view

		if($this->session->userdata('logged_in') == TRUE){

			$data = $this->gaji_model->get_gaji_by_karyawan_id($id);

			$this->load->view('cetak_gaji_view');

		} else {
			redirect('login/index');
		}
	}


}

/* End of file id_kategori.php */
/* Location: ./application/controllers/id_kategori.php */
