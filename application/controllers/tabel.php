<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tabel extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tabel_model');
	}

	public function index()
	{
		
			if($this->session->userdata('logged_in') == TRUE){

			$data['main_view'] = 'tabel';
			$data['tampil_absensi'] = $this->tabel_model->get_data_absensi();	
			$data['top'] = $this->tabel_model->getDataTop();
			$this->load->view('template',$data);

		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in') == TRUE){

			if($this->tabel_model->hapus() == TRUE){
				$this->session->set_flashdata('notif', 'Hapus Absensi Berhasil');
				redirect('tabel/index');
			} else {
				$this->session->set_flashdata('notif', 'Hapus Absensi Gagal');
				redirect('tabel/index');
			}

		} else {
			redirect('login/index');
		}
	}

	public function get_data_absensi_by_id($id)
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data = $this->tabel_model->get_data_absensi_by_id($id);
			echo json_encode($data);

		} else {
			redirect('login/index');
		}
	}
	
}

/* End of file Kasir.php */
/* Location: ./application/controllers/Kasir.php */