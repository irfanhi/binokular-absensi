<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_karyawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('karyawan_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data['main_view'] = 'karyawan_view';
			$data['karyawan'] = $this->karyawan_model->get_karyawan();

			//get_kategori untuk dropdown tambah/edit karyawan
			$data['kategori'] = $this->karyawan_model->get_kategori();
			$this->load->view('template', $data);

		} else {
			redirect('login/index');
		}
	}

	public function tambah()
	{

			if ($this->form_validation->run() == TRUE) {
				//upload file
				$config['upload_path'] = './asset/assets/foto/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '200000000000000000000';
				$this->load->library('upload', $config);
				if($this->upload->do_upload('foto')){
					if($this->karyawan_model->tambah($this->upload->data()) == TRUE)
					{
						$this->session->set_flashdata('notif', 'Tambah Karyawan Berhasil');
						redirect('Daftar_karyawan/index');
					} else {
						$this->session->set_flashdata('notif', 'Tambah Karyawan Gagal');
						redirect('Daftar_karyawan/index');
					}
				} else {
					$this->session->set_flashdata('notif', 'Tambah Karyawan gagal upload');
					redirect('Daftar_karyawan/index');
				}
			} 

			else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('Daftar_karyawan/index');
			}
		} 
	

	public function ubah()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$this->form_validation->set_rules('ubah_karyawan', 'karyawan', 'trim|required');
			$this->form_validation->set_rules('ubah_kategori', 'id_kategori', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if($this->karyawan_model->ubah() == TRUE)
				{
					$this->session->set_flashdata('notif', 'Ubah Karyawan Berhasil');
					redirect('Daftar_karyawan','refresh');
				} else {
					$this->session->set_flashdata('notif', 'Ubah Karyawan Gagal');
					redirect('Daftar_karyawan/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('karyawan/index');
			}


		} else {
			redirect('login/index');
		}
	}

	public function hapus()
	{
		if($this->session->userdata('logged_in') == TRUE){

			if($this->karyawan_model->hapus() == TRUE){
				$this->session->set_flashdata('notif', 'Hapus Karyawan Berhasil');
				redirect('Daftar_karyawan/index');
			} else {
				$this->session->set_flashdata('notif', 'Hapus Karyawan Gagal');
				redirect('Daftar_karyawan/index');
			}

		} else {
			redirect('login/index');
		}
	}

	public function get_data_karyawan_by_id($id)
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data = $this->karyawan_model->get_data_karyawan_by_id($id);
			echo json_encode($data);

		} else {
			redirect('login/index');
		}
	}

}

/* End of file id_kategori.php */
/* Location: ./application/controllers/id_kategori.php */
