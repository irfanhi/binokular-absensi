<div class="page-content-wrap">                
    <div class="row">
        <div class="col-md-12">

<!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title"><strong>Penggajian Karyawan</strong> </h3>
                    <?php
                        $notif = $this->session->flashdata('notif');
                        if($notif != NULL){
                            echo '<div class="alert alert-danger">'.$notif.'</div>';
                        }
                    ?>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>    
            </div>

<div class="row">
    <div class="col-md-12">
        <!-- TABLE STRIPED -->
    <div class="panel">
        <div class="panel-body">
            <table class="table datatable">
                <thead>
                    <tr>
                    <th>No</th>

                    <th>Id Karyawan</th>

                    <th>Nama Karyawan</th>

                    <th>Total Masuk</th>

                    <th>Tunjangan</th>

                    <th>Gaji Utama</th>

                    <th>Total Gaji</th>

                    <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
            <?php
                $no = 1;
                $hari = 20000;
                    foreach ($gaji as $g) {
                $total_gaji = ($g->gaji + ($g->total_absensi * $hari));
                    echo '
                        <tr>
                        <td>'.$no.'</td>
                        <td>'.$g->id_karyawan.'</td>
                        <td>'.$g->karyawan.'</td>
                        <td>'.$g->total_absensi.' hari</td>
                        <td>Rp. '.number_format($g->total_absensi*$hari).'</td>
                        <td>Rp. '.number_format($g->gaji).'</td>
                        <td>Rp. '.number_format($total_gaji).'</td>
                        <td>
                        <a href="#" title="Detil gaji" class="btn btn-warning btn-sm fa fa-tags" data-toggle="modal" data-target="#modal_detil_gaji" onclick="prepare_detil_gaji('.$g->id_karyawan.') " ></a>

                        <a href="gaji/bayar2/? id_karyawan='.$g->id_karyawan.'&bulan='.$g->bulan.'&total_gaji='.$total_gaji.'" class="btn btn-success btn-sm fa fa-money" title="Bayar"></a>

                        </td>                                                                
                        </tr>
                        ';
                    $no++;
                }
            ?>   


                </tbody>
                </table>
                </div>
            </div>
                <!-- END TABLE STRIPED -->
            </div>
        </div>
    </div>
</div>

        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title"><strong>Riwayat Gaji</strong> </h3>
                    <?php
                        $notif = $this->session->flashdata('notif');
                        if($notif != NULL){
                            echo '<div class="alert alert-danger">'.$notif.'</div>';
                        }
                    ?>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>    
            </div>
            <!-- MAIN CONTENT-->

    <div class="row">
        <div class="col-lg-9">
            <div class="table-responsive table--no-card m-b-60">
                                   
                               
            <!-- END DATA TABLE -->
            </div>
        </div>

    <div class="row">
    <div class="col-md-12">
        <!-- TABLE STRIPED -->
    <div class="panel">
        <div class="panel-body">
            <table class="table datatable">
                    <thead>  
                    <tr>
                        <th>No</th>
                            <th>Bulan</th>
                            <th>Tanggal</th>
                            <th>Nama</th>       
                            <th>Gaji Total</th>
                            <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                            foreach ($tampil_bayar as $bayar) {
                        $bulan = date_create($bayar->bulan);
                        $bulan = date_format($bulan, 'F , Y ');
                        $tgl_absensi = date_create($bayar->tgl_absensi);
                        $tgl_absensi = date_format($tgl_absensi, 'd F , Y ');
                            echo '
                                 <tr>
                                    <td>'.$no.'</td>
                                    <td>'.$bulan.'</td>
                                    <td>'.$tgl_absensi.'</td>
                                    <td>'.$bayar->karyawan.'</td>
                                    <td>'.number_format($bayar->total_gaji).'</td>

                                </tr>
                            ';
                        $no++;
                    }

                ?>
                    </tbody>
                </table>
                                    
            </div>
            <!-- END DATA TABLE-->
        </div>
    </div>
                        

<div id="modal_detil_gaji" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detil Gaji</h4>
      </div>

          <div class="modal-body">
                
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-warning" id="cetak_gaji" target="_blank">CETAK NOTA</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

    </div>

  </div>
</div>

<script type="text/javascript">

    function prepare_detil_gaji(id)
    {
        $(".modal-body").empty();
        $.getJSON('<?php echo base_url(); ?>index.php/gaji/karyawan_id/' + id,  function(data){
            $(".modal-body").html(data.show_detil);
        });

        $('#cetak_gaji').attr('href','<?php echo base_url();?>index.php/gaji/cetak_gaji/' + id);
    }
</script>

