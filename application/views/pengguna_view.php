<div class="main-content">
	<div class="container-fluid">
		<div class="row mb-2">
          <div class="col-sm-6">
			<h3 class="panel-title"><strong>Data Pengguna</strong> </h3>
					<?php
			$notif = $this->session->flashdata('notif');
			if($notif != NULL){
				echo '
					<div class="alert alert-danger">'.$notif.'</div>
				';
			}
		?>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
				<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah Pengguna</button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

		<div class="row">
			<div class="col-md-12">
				<!-- TABLE STRIPED -->
				<div class="panel">
					<div class="panel-body">
					  <table class="table datatable">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Username</th>
									<th>Level</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$no = 1;
								foreach ($pengguna as $b) {
									echo '
										<tr>
											<td>'.$no.'</td>
											<td>'.$b->nama.'</td>
											<td>'.$b->username.'</td>
											<td>'.$b->level.'</td>
											<td>
												<a href="#" title="Ubah" class="btn btn-info btn-sm btn-sm fa fa-pencil" data-toggle="modal" data-target="#ubah" onclick="prepare_ubah_pengguna('.$b->id_user.')"></a>
												<a href="#" title="Hapus" class="btn btn-danger btn-sm btn-sm glyphicon glyphicon-trash" data-toggle="modal" data-target="#hapus" onclick="prepare_hapus_pengguna('.$b->id_user.')"></a>
											</td>
										</tr>
									';
									$no++;
								}
							?>
							</tbody>
						</table>

					</div>
				</div>
				<!-- END TABLE STRIPED -->
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah pengguna</h4>
 
      </div>
      <form action="<?php echo base_url('index.php/Daftar_Petugas/tambah'); ?>" method="post">
	      <div class="modal-body">
	        	<input type="text" class="form-control" placeholder="Nama" name="nama">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Username" name="username">
	        	<br>
	        	<input type="password" class="form-control" placeholder="Password" name="password">
	        	<br>
	        	<select class="form-control" name="level">
	        		<option value="admin">Admin</option>
	        	</select>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div id="ubah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	            <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah pengguna</h4>

      </div>
      <form action="<?php echo base_url('index.php/Daftar_Petugas/ubah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="hidden" name="ubah_id" id="ubah_id">
	        	<input required="" type="text" class="form-control" placeholder="Nama" name="ubah_nama" id="ubah_nama">
	        	<br>
	        	<input required="" type="text" class="form-control" placeholder="Username" name="ubah_username" id="ubah_username">
	        	<br>
	        	<input required="" type="password" class="form-control" placeholder="Password" name="ubah_password" id="ubah_password">
	        	<br>
	        	<select class="form-control" name="ubah_level" id="ubah_level">
	        		<option value="admin">Admin</option>
	        		<option value="karyawan">karyawan</option>
	        	</select>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi Hapus pengguna</h4>

      </div>
      <form action="<?php echo base_url('index.php/Daftar_Petugas/hapus'); ?>" method="post">
	      <div class="modal-body">
	        	<input type="hidden" name="hapus_id"  id="hapus_id">
	        	<p>Apakah anda yakin menghapus pengguna <b><span id="hapus_nama"></span></b> ?</p>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-danger" name="submit" value="YA">
	        <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	
	function prepare_ubah_pengguna(id)
	{
		$("#ubah_id").empty();
		$("#ubah_nama").empty();
		$("#ubah_username").empty();
		$("#ubah_password").empty();
		$("#ubah_level").val();

		$.getJSON('<?php echo base_url(); ?>index.php/Daftar_Petugas/get_data_pengguna_by_id/' + id,  function(data){
			$("#ubah_id").val(data.id_user);
			$("#ubah_nama").val(data.nama);
			$("#ubah_username").val(data.username);
			$("#ubah_password").val(data.password);
			$("#ubah_level").val(data.level);
		});
	}

	function prepare_hapus_pengguna(id)
	{
		$("#hapus_id").empty();
		$("#hapus_nama").empty();

		$.getJSON('<?php echo base_url(); ?>index.php/Daftar_Petugas/get_data_pengguna_by_id/' + id,  function(data){
			$("#hapus_id").val(data.id_user);
			$("#hapus_nama").text(data.nama);
		});
	}
</script>
