<div class="page-content-wrap">                
<div class="row">
    <div class="col-md-12">
     <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title"><strong>Tabel Absensi</strong> </h3>
            <?php
                $notif = $this->session->flashdata('notif');
                if($notif != NULL){
                    echo '<div class="alert alert-danger">'.$notif.'</div>';
                }
            ?>
            <ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>    
            </div>
                
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                        <tr>
                        <th>No</th>

                        <th>Id Karyawan</th>
                                                
                        <th>Nama</th>
                                                
                        <th>Tgl.Absensi</th>

                        <th>Jam</th>

                        <th>Absensi</th>

                        <th>Aksi</th>

                        </tr>
                        </thead>
                    <tbody>
                            <?php foreach ($tampil_absensi as $absensi): ?>
                                <tr>
                                <td><?=$absensi->id_absensi?></td>
                                <td><?=$absensi->id_karyawan?></td>
                                <td><?=$absensi->karyawan?></td> 
                                <td><?=$absensi->tgl_absensi?></td>
                                <td><?=$absensi->jam?></td>
                                <td><?=$absensi->absensi?></td>          
                                <td>
                                    <a href="#" title="Hapus" class="btn btn-danger btn-sm glyphicon glyphicon-trash" data-toggle="modal" data-target="#hapus" onclick="prepare_hapus_absensi(<?=$absensi->id_absensi?>)"></a>
                                </td>
                                </tr> 
                            <?php endforeach ?> 
                    </tbody>
                        </table>
                </div>

        <div class="page-content-wrap">                
<div class="row">
    <div class="col-md-12">
     <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title"><strong>Top 5 Absensi</strong> </h3>
            <?php
                $notif = $this->session->flashdata('notif');
                if($notif != NULL){
                    echo '<div class="alert alert-danger">'.$notif.'</div>';
                }
            ?>  
            </div>
                                    <table class="table table-borderless table-data3">
                                        <thead>  
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Absen</th>          
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                              $no = 1;
                                              foreach ($top as $t) {
                                              echo '
                                                <tr>
                                                    <td>'.$no.'</td>
                                                    <td>'.$t->karyawan.'</td>
                                                    <td>'.$t->abs.'</td>
                                                    
                                                </tr>    
                                                    ';    
                                                
                                                $no++; 
                                            } 
                                        ?>                                           
                                        </tbody>   
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                           
                        </div>
                        
                            <div class="panel-footer">                                  
                                <form action="<?php echo base_url('index.php/absensi'); ?>" method="post">               
                                <button class="btn btn-info pull-right">Submit</button>   
                                </form> 
                            </div>
                                        
<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi Hapus Absensi</h4>

      </div>
      <form action="<?php echo base_url('index.php/tabel/hapus'); ?>" method="post">
          <div class="modal-body">
                <input type="hidden" name="hapus_id"  id="hapus_id">
                <p>Apakah anda yakin menghapus Absensi <b><span id="hapus_id_karyawan"></span></b> ?</p>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-danger" name="submit" value="YA">
            <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          </div>
      </form>
    </div>

  </div>
</div>

    <script src="<?=base_url()?>asset2/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/popper.min.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/plugins.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/main.js"></script>     

<script type="text/javascript">
    
    function prepare_hapus_absensi(id)
    {
        $("#hapus_id").empty();
        $("#hapus_absensi").empty();

        $.getJSON('<?php echo base_url(); ?>index.php/tabel/get_data_absensi_by_id/' + id,  function(data){
        $("#hapus_id").val(data.id_absensi);
        $("#hapus_absensi").text(data.absensi);
       
        });
    }
</script>
