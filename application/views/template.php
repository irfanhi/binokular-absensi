<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>BINOKULAR || ABSENSI</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?=base_url();?>asset/favicon.ico" type="image/x-icon" />

        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?=base_url();?>asset/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?=base_url();?>asset/index.html">BINOKULAR</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=base_url();?>asset/assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=base_url();?>asset/assets/images/users/avatar.jpg" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?= $this->session->userdata('username');?></div>
                                <div class="profile-data-title">Absensi Kerja</div>
                            </div>
                        
                        </div>                                                                        
                    </li>


                    <?php if ($this->session->userdata('level')=="admin"): ?>
                    <li class="xn-title">Navigation</li>
                   
                    <li>
                        <a href="<?php echo base_url();?>index.php/absensi"><span class="menu-icon fa fa-th"></span> <span class="xn-text">Entry Absensi</span></a>
                           </li>  
                    <li>
                        <a href="<?php echo base_url();?>index.php/tabel"><span class="menu-icon fa fa-table"></span> <span class="xn-text">List Absensi</span></a>
                           </li>  
                    <li >
                        <a href="<?php echo base_url();?>index.php/gaji"><span class="fa fa-desktop"></span> <span class="xn-text">Penggajian karyawan</span></a></li>  
                           <li >        
                    <li>
                        <a href="<?php echo base_url();?>index.php/Daftar_karyawan"><span class="fa fa-fw fa-file"></span> <span class="xn-text">Daftar Karyawan</span></a>
                           </li>              
                     <li>
                        <a href="<?php echo base_url();?>index.php/kategori"><span class="fa fa-fw fa fa-gavel"></span> <span class="xn-text">Divisi</span></a>
                           </li>         
                    <li>
                        <a href="<?php echo base_url();?>index.php/Daftar_Petugas"><span class="fa fa-fw fa-wrench"></span> <span class="xn-text">Daftar Petugas</span></a>
                           </li>  

                    <?php endif ?>
                    <?php if ($this->session->userdata('level')=="karyawan"): ?> 
                        <li >
                            <a href="<?php echo base_url();?>index.php/Dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a></li>     
                    
                    <?php endif ?> 
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 

                </ul>

                <ul class="breadcrumb">

                    
        <?php
            $tanggal= mktime(date("m"),date("d"),date("Y"));
                echo "Tanggal : <b>".date("d-M-Y", $tanggal)."</b> ";
            date_default_timezone_set('Asia/Jakarta');
            $jam=date("H:i:s");
            $date = date('Y-m-d h:i:s'); 
                echo "<br> Hari : ". hari($date);
        ?> 

        <html>
            <body>
                <font face="Consolas">
                <h1 id="waktu"></h1>
                </font>
            <script type="text/javascript">
                function showClock()
                    {
                        thisTime = new Date();
                        jam = thisTime.getHours();
                        menit = thisTime.getMinutes();
                        detik = thisTime.getSeconds();
                        waktu  = (jam > 9)? jam : "0"+jam;
                        waktu += (menit > 9)? ":"+menit : ":0"+menit;
                        waktu += (detik > 9)? ":"+detik : ":0"+detik;
                document.getElementById("waktu").innerHTML = waktu;
                setTimeout("showClock()", 200);
                    }
                showClock();
            </script>
            </body>
        </html>

<?php

function hari($hari){
  $hari=nama_hari(date('w'));
  return $hari;
}

function nama_hari($nama_hari){
   switch($nama_hari){      
        case 0 : 
             return 'Ahad';
             break;
        case 1 : 
           return 'Senin';
            break;
        case 2 : 
            return 'Selasa';
            break;
        case 3 : 
            return 'Rabu';
            break;
        case 4 : 
            return 'Kamis';
            break;
        case 5 : 
            return "Jum'at";
            break;
        case 6 : 
           return 'Sabtu';
            break;
  }
}

?>
    
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <?php
                        $this->load->view($main_view);
                     ?>
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title" ><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo base_url(); ?>index.php/login/logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?=base_url();?>asset/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="<?=base_url();?>asset/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- START SCRIPTS PRINT -->
    <script src="<?=base_url()?>asset/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>asset/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- END --!>


        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?=base_url();?>asset/js/settings.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins.js"></script>        
        <script type="text/javascript" src="<?=base_url();?>asset/js/actions.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/demo_dashboard.js"></script>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->     
        
    </body>
</html>






