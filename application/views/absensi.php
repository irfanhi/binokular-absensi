<div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal"  action="<?php echo base_url('index.php/absensi/tambah'); ?>" method="post">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Entry Absensi</strong> </h3>
                    <?php
                        $notif = $this->session->flashdata('notif');
                        if($notif != NULL){
                            echo '<div class="alert alert-danger" >'.$notif.'</div>';
                        }
                    ?>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                        </div>
                            <div class="panel-body">
                                <p>Selamat Bekerja Mbbak Sevii</p>
                            </div>
                                                    
                <div class="panel-body">                                                                        
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Nama</label>
                            <div class="col-md-6 col-xs-12">                                            
                                <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>        
                                <select required="" name="id_karyawan" class="form-control">
                                <option disabled>Pilih karyawan</option>
                                    <?php foreach ($data_karyawan as $karyawan): ?>
                                <option value="<?=$karyawan->id_karyawan?>"><?=$karyawan->karyawan?></option>
                                    <?php endforeach ?>
                                </select>
                                </div> 

                            <span class="help-block">This is sample of text field</span>
                        </div>
                    </div>
                                          
                            <div class="form-group">                                        
                                <label class="col-md-3 col-xs-12 control-label">Tgl.Kehadiran</label>
                                    <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input required="" type="text" class="form-control datepicker" name="tgl_absensi">                                       
                                        </div>
                                    <span class="help-block">Click on input field to get datepicker</span>
                                </div>
                            </div>
                                      
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Absensi</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <div class="col-md-4">                                    
                                            <label class="check"><input type="radio" class="radio-btn" value="Yes" name="absensi" /> Yes</label>
                                        </div>
                                        <div class="col-md-4">                                    
                                            <label class="check"><input type="radio" class="radio-btn" value="No" name="absensi"/> No</label>
                                        </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">                                   
                                    <button class="btn btn-primary pull-right" name="Submit" type="submit">Submit</button>
                                </div>
                                      </form>

                                      
                            </div>
                            </div>  
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>

    <script src="<?=base_url()?>asset2/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/popper.min.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/plugins.js"></script>
    <script src="<?=base_url()?>asset2/assets/js/main.js"></script>