<div class="main-content">
	<div class="container-fluid">
		<div class="row mb-2">
          <div class="col-sm-6">
			<h3 class="panel-title"><strong>Daftar Karyawan</strong> </h3>
					<?php
			$notif = $this->session->flashdata('notif');
			if($notif != NULL){
				echo '
					<div class="alert alert-danger">'.$notif.'</div>
				';
			}
		?>
			</div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button  type="button"  class="btn btn-success btn-sm"  data-toggle="modal" data-target="#tambah">Tambah Karyawan</button> 
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
          </div><!-- /.col -->
        </div><!-- /.row -->


		<div class="row">
			<div class="col-md-12">
				<!-- TABLE STRIPED -->
				<div class="panel">
					<div class="panel-body">
				    <table class="table datatable">
							<thead>
								<tr>
									<th>No</th>
									<th>Id</th>
									<th>Foto</th>
									<th>Nama Karyawan</th>
									<th>No.Telepon</th>
									<th>Divisi</th>
									<th>Gaji</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$no = 1;
								foreach ($karyawan as $b) {
									echo '
										<tr>
											<td>'.$no.'</td>
											<td>'.$b->id_karyawan.'</td>
											<td><img src="'.base_url().'asset/assets/foto/'.$b->foto.'" width="50px" /></td>
											<td>'.$b->karyawan.'</td>
											<td>'.$b->no_telepon.'</td>
											<td>'.$b->nama_kategori.'</td>
											<td>'.$b->gaji.'</td>
											<td>
												<a href="#" title="Ubah" class="btn btn-info btn-sm fa fa-pencil" data-toggle="modal" data-target="#ubah" onclick="prepare_ubah_karyawan('.$b->id_karyawan.')"></a>
												<a href="#" title="Hapus" class="btn btn-danger btn-sm glyphicon glyphicon-trash" data-toggle="modal" data-target="#hapus" onclick="prepare_hapus_karyawan('.$b->id_karyawan.')"></a>
											</td>
										</tr>
									';
									$no++;
								}
							?>
								
							</tbody>
						</table>

					</div>
				</div>
				<!-- END TABLE STRIPED -->
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Karyawan</h4>


      </div>
      <form action="<?php echo base_url('index.php/Daftar_karyawan/tambah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Nama Karyawan" name="karyawan">
	        	<br>
	        	<input type="text" class="form-control" placeholder="no-stelepon" name="no.telepon">
	        	<br>
	        	<input type="text" class="form-control" placeholder="gaji" name="gaji">
	        	<br>
	        	<select name="kategori" class="form-control">
					<?php
						foreach ($kategori as $k) {
							echo '
								<option value="'.$k->id_kategori.'">'.$k->nama_kategori.'</option>
							';
						}
					?>	        		
	        	</select>
	        	<br>
	        	<input type="file" class="form-control" placeholder="Foto" name="foto">

	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>

  </div>
</div>

<div id="ubah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Karyawan</h4>

      </div>
      <form action="<?php echo base_url('index.php/Daftar_karyawan/ubah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="hidden" name="ubah_id_karyawan"  id="ubah_id_karyawan">
	        	<input required="" type="text" class="form-control" placeholder="Id Karyawan" name="ubah_kode_karyawan"  id="ubah_kode_karyawan" disabled>
	        	<br>
	        	<input required="" type="text" class="form-control" placeholder="Nama Menu" name="ubah_karyawan"  id="ubah_karyawan">
	        	<br>
	        	<input required="" type="text" class="form-control" placeholder="no-stelepon" name="no.telepon">
	        	<br>
	        	<input required="" type="text" class="form-control" placeholder="gaji" name="gaji">
	        	<br>
	        	<select name="ubah_kategori" id="ubah_kategori" class="form-control">
					<?php
						foreach ($kategori as $k) {
							echo '
								<option value="'.$k->id_kategori.'">'.$k->nama_kategori.'</option>
							';
						}
					?>	        		
	        	</select>
	        	<br>
	        	<center><div id="data_foto"></div></center>
	      </div>
	     <div class="modal-footer">
                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </form>
    </div>
  </div>
</div>

<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi Hapus Karyawan</h4>

      </div>
      <form action="<?php echo base_url('index.php/Daftar_karyawan/hapus'); ?>" method="post">
	      <div class="modal-body">
	        	<input type="hidden" name="hapus_id"  id="hapus_id">
	          	<p>Apakah anda yakin menghapus karyawan <b><span id="hapus_karyawan"></span></b> ?</p>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-danger" name="submit" value="YA">
	        <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
	      </div>
      </form>
    </div>

  </div>
</div>

<script type="text/javascript">
	
	function prepare_ubah_karyawan(id)
	{
		$("#ubah_id_karyawan").empty();
		$("#ubah_kode_karyawan").empty();
		$("#ubah_karyawan").empty();;
		$("#ubah_kategori").val();
		$("#data_foto").empty();

		$.getJSON('<?php echo base_url(); ?>index.php/Daftar_karyawan/get_data_karyawan_by_id/' + id,  function(data){
			$("#ubah_id_karyawan").val(data.id_karyawan);
			$("#ubah_kode_karyawan").val(data.id_karyawan);
			$("#ubah_karyawan").val(data.karyawan);
			$("#ubah_kategori").val(data.id_kategori);
			$("#data_foto").html('<img src="<?php echo base_url(); ?>asset/assets/foto/' + data.foto + '" width="150px" >');
		});
	}

	function prepare_hapus_karyawan(id)
	{
		$("#hapus_id").empty();
		$("#hapus_karyawan").empty();

		$.getJSON('<?php echo base_url(); ?>index.php/Daftar_karyawan/get_data_karyawan_by_id/' + id,  function(data){
			$("#hapus_id").val(data.id_karyawan);
			$("#hapus_karyawan").text(data.karyawan);
		});
	}
</script>


