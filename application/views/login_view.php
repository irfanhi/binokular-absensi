<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>BINOKULAR || ABSENSI</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?=base_url();?>asset/favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?=base_url();?>asset/css/theme-default.css"/>
        <link rel="stylesheet" type="text/css" id="bootstrap" href="<?=base_url()?>asset/css/bootstrap/bootstrap.min.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>,Please Login</div>

                   <?php
                        if(!empty($notif))
                        {
                            echo '<div class="alert alert-danger">';
                            echo $notif;
                            echo '</div>';
                        }
                    ?>
                <form method="post" action="<?php echo base_url(); ?>index.php/login/cek_login">
                    <input type="text" class="form-control" name="username" placeholder="Username" required=""
                    style="padding:16">
                    <br>
                    <input type="password" class="form-control" name="password" class="lock" placeholder="Password" required="" style="padding:16;">
                    <br> 
                    <input type="submit" class="btn btn-default" name="login" value="Login">    
                    
                </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2018 Binokular
                    </div>
                    <div class="pull-right">
                        <a href="">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a>
                    </div>
                </div>
            </div>
            
        </div>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src='<?=base_url();?>asset/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?=base_url();?>asset/js/settings.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/plugins.js"></script>        
        <script type="text/javascript" src="<?=base_url();?>asset/js/actions.js"></script>
        
        <script type="text/javascript" src="<?=base_url();?>asset/js/demo_dashboard.js"></script>
    </body>
</html>






